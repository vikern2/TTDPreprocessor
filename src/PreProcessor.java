public class PreProcessor {
    Node lookupTable[];
    private int capacity;
    int size;
    private IFileHandler handler;

    public PreProcessor(IFileHandler handler, int capacity){
        this.handler = handler;
        this.capacity = capacity;
        lookupTable = new Node[capacity];
    }

    public int genHashValue(String s){
        int hashvalue = 0;
        for(int i = 0; i<s.length(); i++){
            hashvalue += s.charAt(i) + 31*hashvalue;
        }return Math.abs(hashvalue % lookupTable.length);
    }

    public boolean install(String s, String v) {
        if(load() > 0.75){
            rehash();
        }
        if(contains(s)){
            return false;
        }
        int hv = genHashValue(s);
        if(lookupTable[hv] == null) {
            lookupTable[hv] = new Node(s, v);
            size++;
            return true;
        }else if(hasNext(lookupTable[hv])){
            Node tmp = findSecondLast(lookupTable[hv]);
            (tmp.next).next = new Node(s, v);
            size++;
            return true;
        }else{
            lookupTable[hv].next = new Node(s, v);
            size++;
            return true;
        }
    }

    public int rehash() {
        capacity = (lookupTable.length)*2;
        Node[] newLookupTable = new Node[capacity];
        Node[] tmpLookupTable = lookupTable;
        lookupTable = newLookupTable;
        size = 0;
        for (int i = 0; i < tmpLookupTable.length - 1; i++) {
            if (tmpLookupTable[i] != null) {
                install(tmpLookupTable[i].symbol, tmpLookupTable[i].value);
                Node current = tmpLookupTable[i];
                while (hasNext(current)) {
                    install((current.next).symbol, (current.next).value);
                    current = current.next;
                }
            }
        }return capacity;
    }

    public int undef(String s){
        if(contains(s)){
            int hv = genHashValue(s);
            Node current = lookupTable[hv];
            if(current.symbol.equals(s)){
                if (hasNext(current)){
                    lookupTable[hv] = current.next;
                }else{
                    lookupTable[hv] = null;
                }
            }else{
                while (!(current.next).symbol.equals(s)){
                    current = current.next;
                }
                if(hasNext(current.next)){
                    current.next = (current.next).next;
                    }else{
                    current.next = null;
                }
            }
            size--;
        }return size;
    }

    public double load(){
        return (double) size /lookupTable.length;
    }

    public boolean contains(String s) {
        int hv = genHashValue(s);
        if(lookupTable[hv] != null){
            Node current = lookupTable[hv];
            if(current.symbol.equals(s)){
                return true;
            }
            while(hasNext(current)){
                if(current.symbol == s){
                    return true;
                }else{
                    current = current.next;
                }
            }if(current.symbol.equals(s)){
                return true;
            }
        }return false;
    }

    public Node findSecondLast(Node node){
        Node current = node;
        while(hasNext(current) && hasNext(current.next)){
            current = current.next;
        }return current;
    }

    public String lookup(String s){
        int hv = genHashValue(s);
        Node current = lookupTable[hv];
        if(contains(s)){
            if(current.symbol.equals(s)) {
                return current.value;
            }
            while(current.next != null){
                if(current.symbol.equals(s)){
                    return current.value;
                }else{
                    current = current.next;
                }
            }
        }return "Symbol " + s + " does not exist";
    }

    public boolean hasNext(Node node){
        return (node.next != null);
    }

    public String readNextSymbol(){
        String tmp = handler.nextLine();
        String[] ssTmp = tmp.split(" ");
        if(tmp.startsWith("#define")){
            return tmp.substring(8);
        }else if(tmp.startsWith("#")){
            return ssTmp[0];
        }return "";
    }

    public boolean isEmpty(){
        return (size == 0);
    }

    public int getSize(){
        return size;
    }

    public int getCapacity(){
        return capacity;
    }

}
