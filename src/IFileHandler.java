
public interface IFileHandler {
    public boolean openFile(String name);
    public String nextLine();
    public boolean hasMore();
}
