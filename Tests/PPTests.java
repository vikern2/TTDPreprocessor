import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class PPTests {
    PreProcessor pp;
    IFileHandler handler;

    @Before
    public void setUp(){
        handler = mock(IFileHandler.class);
        pp = new PreProcessor(handler, 10);
    }


    @Test
    public void HashVal(){
        assertEquals(2, pp.genHashValue("ab"));
    }

    @Test
    public void contains(){
        pp.install("ab", "12");
        assertEquals(true, pp.contains("ab"));
    }

    @Test
    public void dontAddIfContains(){
        pp.install("ab", "12");
        assertEquals(false, pp.install("ab", "12"));
    }

    @Test
    public void canAddIfMoreThanTwoNodesAtIndex() {
        pp.install("asde", "17");
        pp.install("abq", "hei");
        pp.install("asdo", "1");
        assertEquals("abq", pp.findSecondLast(pp.lookupTable[7]).symbol);
        assertEquals("asdo", pp.findSecondLast(pp.lookupTable[7]).next.symbol);
    }

    @Test
    public void canAddIfMoreThanThreeNodesAtIndex(){
        pp.install("asde", "17");
        pp.install("abq", "hei");
        pp.install("asdo", "1");
        pp.install("asdy", "3");
        assertEquals("asdo", pp.findSecondLast(pp.lookupTable[7]).symbol);
        assertEquals("asdy", pp.findSecondLast(pp.lookupTable[7]).next.symbol);
    }

    @Test
    public void undefineNode(){
        pp.install("asde", "17");
        pp.install("abq", "hei");
        pp.install("asdo", "1");
        pp.undef("abq");
        assertEquals("asde", pp.findSecondLast(pp.lookupTable[7]).symbol);
        assertEquals("asdo", pp.findSecondLast(pp.lookupTable[7]).next.symbol);
        pp.undef("asde");
        assertEquals("asdo", pp.lookupTable[7].symbol);
        pp.undef("asdo");
        assertEquals(0, pp.size);
    }

    @Test
    public void undefLast(){
        pp.install("asde", "17");
        pp.install("abq", "hei");
        pp.install("asdo", "1");
        pp.undef("asdo");
        assertEquals("abq", pp.findSecondLast(pp.lookupTable[7]).next.symbol);
    }

    @Test
    public void lookup(){
        pp.install("ac", "11");
        pp.install("asd", "1");
        assertEquals("11", pp.lookup("ac"));
    }

    @Test
    public void lookupWithMoreNodesAtIndex(){
        pp.install("asde", "17");
        pp.install("abq", "hei");
        pp.install("asdo", "1");
        pp.install("asdy", "3");
        assertEquals("1", pp.lookup("asdo"));
    }

    @Test
    public void lookupWithNoNodesAtIndex(){
        assertEquals("Symbol ase does not exist", pp.lookup("ase"));
    }

    @Test
    public void install(){
        assertEquals(false, pp.contains("ab"));
        pp.install("ab", "11");
        assertEquals(true, pp.contains("ab"));
    }

    @Test
    public void rehash(){
        pp.install("aber", "11");
        pp.install("asdqqq", "1");
        pp.install("asdffe", "1");
        pp.install("ab", "11");
        pp.install("asde", "1");
        pp.install("abq", "11");
        pp.install("asdee", "1");
        pp.install("abf", "11");
        pp.install("asdg", "1");
        pp.install("asdgg", "1");
        assertEquals(20, pp.lookupTable.length);
    }

    @Test
    public void readNextDefine(){
        when(handler.nextLine()).thenReturn("#define IN 1");
        assertEquals("IN 1", pp.readNextSymbol());
    }

    @Test
    public void readNextPpDirective(){
        when(handler.nextLine()).thenReturn("#hei IN 1");
        assertEquals("#hei", pp.readNextSymbol());
    }

    @Test
    public void readNextNotPpDirective(){
        when(handler.nextLine()).thenReturn("hei IN 1");
        assertEquals("", pp.readNextSymbol());
    }

    @Test
    public void checkEmpty_NoneInstalled(){
        assertEquals(true, pp.isEmpty());
    }

    @Test
    public void checkEmpty_SomeInstalled(){
        pp.install("hei", "12");
        pp.install("hallo", "13");
        assertEquals(false, pp.isEmpty());
    }

    @Test
    public void checkSize_NoneInstalled(){
        assertEquals(0, pp.getSize());
    }

    @Test
    public void checkSize_SomeInstalled(){
        pp.install("hei", "12");
        pp.install("hallo", "13");
        assertEquals(2, pp.getSize());
    }

    @Test
    public void checkCapacity_NoneInstalled(){
        assertEquals(10, pp.getCapacity());
    }

    @Test
    public void checkCapacity_SomeInstalled(){
        pp.install("hei", "12");
        pp.install("hallo", "13");
        assertEquals(10, pp.getCapacity());
    }

    @Test
    public void checkCapacity_ReHashInitiated(){
        pp.install("aber", "11");
        pp.install("asdqqq", "1");
        pp.install("asdffe", "1");
        pp.install("ab", "11");
        pp.install("asde", "1");
        pp.install("abq", "11");
        pp.install("asdee", "1");
        pp.install("abf", "11");
        pp.install("asdg", "1");
        pp.install("asdgg", "1");
        assertEquals(20, pp.getCapacity());

    }


}
